import functools
from typing import List

''' To run doctests for this module.

python3 -m doctest -v helpers/helperslib/Version.py
'''

SUFFIX2NUMBER = {
        "alpha": 70,
        "a": 70,
        "beta": 80,
        "b": 80,
        "rc": 90
}


@functools.total_ordering
class Version:
    '''class for Version and correct ordering. '''

    def __init__(self, version: str) -> None:
        if version.startswith("v"):
            self.version = version[1:]
        else:
            self.version = version
        self._parts = None
        self._suffix = None

    def parts(self) -> List:
        '''returns a list with different parts of version.

        >>> Version("1.2.3-rc1").parts()
        [1, 2, 3]
        '''
        if self._parts:
            return self._parts

        ps = self.version.split(".")

        self._parts = []

        for p in ps:
            try:
                self._parts.append(int(p))
            except ValueError:
                v, suffix = p.split("-")
                self._parts.append(int(v))
                self._suffix = suffix

        return self._parts

    def suffix(self) -> int:
        '''transforms suffix into integer using SUFFIX2NUMBER.
        >>> Version("1.2-alpha").suffix()
        70
        >>> Version("1.2-beta").suffix()
        80
        >>> Version("1.2-rc").suffix()
        90
        >>> Version("1.2-rc1").suffix()
        91
        '''

        if not self._parts:
            self.parts()

        #no suffix so we are higher than any suffix
        if not self._suffix:
            return 100

        for key, base in SUFFIX2NUMBER.items():
            if self._suffix.startswith(key):
                addition = 0
                # handle case with rc1 or beta2 etc.
                if len(self._suffix) > len(key):
                    addition = int(self._suffix[len(key):])
                return base + addition

    def __eq__(self, other):
        '''returns True if versions are the same.

        >>> Version("1.2") == "1.2"
        True

        The KDE logic that 5.3.90 == "5.4-rc" is not implemented!

        >>> Version("5.4-rc") == Version("5.3.90")
        False
        '''
        if hasattr(other, "version"):
            return self.version == other.version
        elif type(other) == str:
            return self == Version(other)
        else:
            NotImplemented

    def __lt__(self, other):
        '''returns True if self < other.

        >>> Version("1.2.3-rc1") < Version("1.2.3")
        True
        >>> Version("1.2.3") < Version("1.2.3-rc1")
        False
        >>> Version("1.2.3") < Version("1.2.4")
        True
        >>> Version("1.2.3") < Version("1.2.3")
        False
        >>> Version("1.2") < Version("1.2.3")
        True
        >>> Version("1.2.3") < Version("1.2")
        False

        The KDE logic that 5.3.89 < "5.4-rc" < 5.3.91 is not implemented!
        Currently:
        >>> Version("5.3.89") < "5.4-rc"
        True
        >>> Version("5.4-rc") < "5.3.91"
        False
        >>> Version("5.3.89") < "5.3.91"
        True
        '''
        if type(other) == str:
            return self < Version(other)
        if hasattr(other, "parts"):
            parts = self.parts()
            otherParts = other.parts()

            for i, part in enumerate(parts):
                try:
                    if part == otherParts[i]:
                        continue
                    return part < otherParts[i]
                except IndexError:
                    # @other has less parts than @self.
                    return False
            if len(parts) < len(otherParts):
                return True
            return self.suffix() < other.suffix()

    def __repr__(self):
        parts = self.parts()
        version = ".".join([str(i) for i in parts])
        if self._suffix:
            version += "-{}".format(self._suffix)

        return "<{}>".format(version)
