#!/usr/bin/python3
import os
import sys
import shutil
import tarfile
import argparse
from helperslib import BuildSpecs, CommonUtils, Buildable, Packages

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to execute tests for a build.')
parser.add_argument('--product', type=str, required=True)
parser.add_argument('--project', type=str, required=True)
parser.add_argument('--branchGroup', type=str, required=True)
parser.add_argument('--platform', type=str, required=True)
parser.add_argument('--environment', type=str, required=True)
parser.add_argument('--installTo', type=str, required=True)
arguments = parser.parse_args()

# Load our build specification, which governs how we handle this build
buildSpecification = BuildSpecs.Loader( product=arguments.product, project=arguments.project, branchGroup=arguments.branchGroup, platform=arguments.platform )

# Initialise the dependnecy resolver
resolver = Buildable.DependencyResolver()

# Ask the resolver to load the list of projects...
projectsTreeLocation = os.path.join( CommonUtils.scriptsBaseDirectory(), 'repo-metadata', 'projects' )
resolver.loadProjectsFromTree( projectsTreeLocation )

# Now ask it to load the list of projects to ignore
ignoreFileLocation = os.path.join( CommonUtils.scriptsBaseDirectory(), 'kde-build-metadata', 'build-script-ignore' )
resolver.loadProjectsIgnoreList( ignoreFileLocation )
# As well as our local ignore file
ignoreFileLocation = os.path.join( CommonUtils.scriptsBaseDirectory(), 'local-metadata', 'ignored-projects' )
resolver.loadProjectsIgnoreList( ignoreFileLocation )

# Make sure the Platform specific ignore rules are loaded as well
ignoreRulesLocation = os.path.join( CommonUtils.scriptsBaseDirectory(), 'local-metadata', 'project-ignore-rules.yaml' )
resolver.loadProjectsIgnoreRules( ignoreRulesLocation )

# Now get it to load the list of dependencies - we have first the common file...
dependenciesFile = os.path.join( CommonUtils.scriptsBaseDirectory(), 'kde-build-metadata', 'dependency-data-common' )
resolver.loadDependenciesFromFile( dependenciesFile )
# And then the branch group specific file...
dependenciesFile = os.path.join( CommonUtils.scriptsBaseDirectory(), 'kde-build-metadata', 'dependency-data-' + arguments.branchGroup )
resolver.loadDependenciesFromFile( dependenciesFile )

# If it exists, we should load an OS specific file as well
dependenciesFile = os.path.join( CommonUtils.scriptsBaseDirectory(), 'local-metadata', 'dependency-' + sys.platform )
if os.path.exists( dependenciesFile ):
	resolver.loadDependenciesFromFile( dependenciesFile )

# Now that we are all initialised, we can ask the resolver for the project we represent, then resolve it's dependencies
resolverProject = resolver.retrieveProject( arguments.project )
projectDependencies = resolver.forProject( resolverProject, arguments.platform )

# Does the install directory exist? If not, make it so!
if not os.path.isdir( arguments.installTo ):
	os.makedirs( arguments.installTo )

# Initialize the archive manager
packageArchive = Packages.Archive( arguments.environment, arguments.platform )

# Go over all of our dependencies, and extract each one in turn into the install directory
for dependency in projectDependencies:
	# Get the package name of this dependency
	package = Packages.nameForProject( arguments.product, dependency.name, arguments.branchGroup )
	# For informative purposes
	print("Retrieving: " + package)

	# Retrieve it from the archive
	filename, metadata = packageArchive.retrievePackage( package )
	# Did we get a failed result?
	if filename is None:
		print("Unable to fetch: " + package)
		sys.exit(1)

	# Open the archive file
	archive = tarfile.open( name=filename, mode='r' )
	# Extract it's contents into the install directory
	archive.extractall( path=arguments.installTo )

# Setup complete!
sys.exit(0)
