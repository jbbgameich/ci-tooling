== Setup of a Windows Host:

-- Necessary Software:

1) Download Python 3.6 for Windows and install it system-wide (the installer has to be run with Administrator rights). Make sure you check the option to set PATH up.
2) Download Git for Windows, installing it system-wide as well, and also ensuring the option to set PATH up is also checked.
3) Download CMake for Windows, also install system-wide, and also ensure it sets up PATH too
4) Install Java from Oracle
5) Install MSVC 2017 Enterprise from Microsoft. Use your best judgement when making the selections - you want the Desktop / C++ / MVC Options
6) Install Qt from the Qt site. The default selections in terms of what you want it to drag down are correct. We are only interested in Desktop compilation (not Universal)

-- Setting up Python

The below will need to be run from an Elevated / Administrator prompt as we use a system-wide Python install

1) Use pip to install lxml and paramiko
2) Download the wheel for PyYAML from http://www.lfd.uci.edu/~gohlke/pythonlibs/#pyyaml and use pip to install it
3) Change the following GPO to make sure Python can handle long paths: https://docs.python.org/3/using/windows.html#max-path

-- Setting up Craft

1) Create C:\Craft
2) Clone git://anongit.kde.org/craft/ into this directory (you should now have C:\Craft\craft\
3) Create C:\Craft\etc\ and copy "kdesettings.ini" from the craft.git checkout there
4) Edit the newly copied kdesettings.ini as follows:
   Set the compiler to "msvc2015"
   Leave Native=False as is
   Set Python to where Python is installed. I ended up using: C:\PROGRA~2\Python36-32\ but your path may differ
   Set DownloadDir to C:\Craft\Download
   Comment UseNinja and MakeProgram as needed
   Set EMERGE_USE_SHORT_PATH to False
   Under [QtSDK]:
     Set Enabled to True
     Make Path match where you installed Qt (this was C:\Qt\ for me)
     The compiler should be "msvc2015_64"

Note: When you wish to install packages, ignore the Craft instructions to use Powershell, I never got it to find MSVC because it's environment script wasn't run
Fortunately the cmd/bat method still works fine, so you should launch the appropriate MSVC shell then run:
    cd C:\Craft\craft\
    call .\kdeenv.bat

After that you should be free to install packages as detailed below.
Craft will drag down it's own copies of Git, CMake, etc - just ignore those as they're harmless.

-- Setting up the Jenkins Slave connection

1) Run "control userpasswords2" and create an account for Jenkins agent to run as. Give it a password (note it for later)
2) Setup the agent on the Jenkins side, selecting a JNLP Webstart agent. I used C:\JenkinsWS\ for it's remote workspace 
   (keep the path short as Windows freaks out easily with path lengths)
3) Launch the agent
4) Select File -> Install as Windows Service.
   This is basically guaranteed to fail, however it does mean the necessary files have now been downloaded to the system.
   You can now close the agent you opened.
5) In an Elevated / Administrator command prompt, switch to the remote workspace specified earlier
   A file called "jenkins-slave.exe" should be present here.
   Run "jenkins-slave.exe install" to setup service based launching
6) Run "services.msc" and find the Jenkins Slave service (name will differ, always starts with Jenkins though)
   Open the service properties, and select the Logon tab.
   Fill in the details of the Jenkins user you created in Step 1 above and confirm.
   Windows will say that the user has been granted the ability to run as a service (or something equivalent) which is fine.

== Craft packages installed:

-- Please note which project it was installed for

jom (General CI System - Parallel Compilation)
libarchive (KArchive)
liblzma (KArchive)
gettext (KI18n)
gettext-tools (KI18n)
winflexbison (Solid)
libxslt (KDocTools)
docbook-dtd (KDocTools)
docbook-xsl (KDocTools)
gcrypt (KWallet)
pcre (KJS)
libpng (KHTML)
libjpeg-turbo (KHTML)
giflib (KHTML)
boost (KActivities)
gperf (KCodecs and KHTML)
docbook-dtd42 (KDELibs4Support)
lmdb (Baloo)
clang (KDevelop)
gsl (Labplot)
